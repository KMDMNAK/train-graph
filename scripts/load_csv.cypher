LOAD CSV WITH HEADERS FROM 'file:///station20210312free.csv' AS station
CREATE (:Station {name: station.station_name, station_cd: station.station_cd, g_cd: station.station_g_cd})
CREATE CONSTRAINT station_cd ON (n:Station) ASSERT n.station_cd IS UNIQUE


LOAD CSV WITH HEADERS FROM 'file:///line20210312free.csv' AS line
CREATE (:Line {name: line.line_name, line_cd: line.line_cd })
CREATE CONSTRAINT line_cd ON (n:Line) ASSERT n.line_cd IS UNIQUE



LOAD CSV WITH HEADERS FROM 'file:///join20210312.csv' AS join
MATCH
  (a:Station),
  (b:Station)
WHERE a.station_cd = join.station_cd1 AND b.station_cd = join.station_cd2

MATCH (line:Line {line_cd: join.line_cd} )

CREATE (a)-[r:RELTYPE {name:line.name, line_cd: join.line_cd} ]->(b)