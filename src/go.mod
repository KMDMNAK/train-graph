module github.com/kmd/train

go 1.16

require (
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/neo4j/neo4j-go-driver/v4 v4.3.0 // indirect
)
