package env

import (
	"fmt"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

type EnvProvider struct {
	NEO4J_PORT     int
	NEO4J_HOST     string
	NEO4J_USER     string
	NEO4J_PASSWORD string
	NEO4j_DB       string
}

func GetEnvProvider() *EnvProvider {
	err := godotenv.Load(fmt.Sprintf(".env"))
	if err != nil {
		fmt.Println(err)
	}
	NEO4J_PORT, err := strconv.Atoi(os.Getenv("NEO4J_PORT"))
	if err != nil {
		NEO4J_PORT = 0
	}
	env := EnvProvider{
		NEO4J_PORT:     NEO4J_PORT,
		NEO4J_HOST:     os.Getenv("NEO4J_HOST"),
		NEO4J_USER:     os.Getenv("NEO4J_USER"),
		NEO4J_PASSWORD: os.Getenv("NEO4J_PASSWORD"),
		NEO4j_DB:       os.Getenv("NEO4j_DB"),
	}
	return &env
}
